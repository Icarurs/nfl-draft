class ResultsController < ApplicationController
  def index
    @drafts = Draft.all
    @last_three = Draft.last_three
  end

  def draft
    if request.post?
      Draft.draft(params[:player_id])
      redirect_to players_path
    end
  end

  def round
    @last_three = Draft.last_three
    @drafts = Draft.where(round: params[:round])

    if @drafts.empty?
      redirect_to results_path
    end
  end
end
