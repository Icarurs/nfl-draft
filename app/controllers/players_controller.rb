class PlayersController < ApplicationController
  def index
    @players = Player.all.sort_by{ |x| [x.position, x.last_name] }
  end

  def available
    @available = Player.available.sort_by{ |x| [x.position, x.last_name] }
  end
end
