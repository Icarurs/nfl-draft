class Player < ActiveRecord::Base
  has_one :draft
  has_one :team, through: :draft

  validates :name, :position, presence: true

  def self.available
    Player.all.select { |player| player.team.nil? }
  end

  def last_name
    self.name.split(' ')[1]
  end

  def drafted?
    self.team.nil? ? false : true
  end

  def self.random_available_player
    Player.available.sample
  end
end
