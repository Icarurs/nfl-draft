class Team < ActiveRecord::Base
  has_many :drafts
  has_many :players, through: :drafts

  validates :name, :division, presence: true

  def self.translate name
    translator = {'NY Giants': 'New York Giants', 'NY Jets': 'New York Jets'}
    translator[name].nil? ? name : translator[name]
  end
end
