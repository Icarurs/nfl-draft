class Draft < ActiveRecord::Base
  belongs_to :team
  belongs_to :player

  validates :round, :pick, :team_id, presence: true

  def self.max_round
    Draft.maximum("round")
  end

  def self.next_team
    if Draft.next_draft.nil?
      return nil
    else
      Draft.next_draft.team
    end
  end

  def self.next_draft
    Draft.where(player_id: nil).first
  end

  def self.draft(player_id)
    draft = Draft.next_draft

    draft.player = Player.find(player_id)
    draft.save!
  end

  def self.complete
    Draft.where('player_id IS NOT NULL').sort_by{:id}
  end

  def self.in_progress
    Draft.where(player_id: nil).sort_by{:id}
  end

  def complete?
    self.player.nil? ? false : true
  end

  def self.last_three
    Draft.complete.last(3)
  end
end
