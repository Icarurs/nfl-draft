require 'test_helper'

class DraftTest < ActiveSupport::TestCase
  def setup
    @draft = Draft.new(round: 1, pick: 1, team_id: 1, player_id: 1)
  end

  test "should be valid" do
    assert @draft.valid?
  end

  test "round should be present" do
    @draft.round = nil
    assert_not @draft.valid?
  end

  test "pick should be present" do
    @draft.pick = nil
    assert_not @draft.valid?
  end

  test "team should be present" do
    @draft.team_id = nil
    assert_not @draft.valid?
  end
end
