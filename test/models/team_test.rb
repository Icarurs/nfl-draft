require 'test_helper'

class TeamTest < ActiveSupport::TestCase
  def setup
    @team = Team.new(name: "Arizona Cardinals", division: "NFC West")
  end

  test "should be valid" do
    assert @team.valid?
  end

  test "name should be present" do
    @team.name = nil
    assert_not @team.valid?
  end

  test "division should be present" do
    @team.division = nil
    assert_not @team.valid?
  end
end
