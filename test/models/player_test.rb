require 'test_helper'

class PlayerTest < ActiveSupport::TestCase
  def setup
    @player = Player.new(name: "Sam Bradford", position: "QB")
  end

  test "should be valid" do
    assert @player.valid?
  end

  test "name should be present" do
    @player.name = nil
    assert_not @player.valid?
  end

  test "position should be present" do
    @player.position = nil
    assert_not @player.valid?
  end
end
