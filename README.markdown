# README

* First perform a database migration with `rake db:migrate`.
* Seed the database with `rake db:seed`.
* Start the application with `rails server`.
* Simulate the draft with `rake draft`.
* Tests can be run with `bundle exec rake test`.
