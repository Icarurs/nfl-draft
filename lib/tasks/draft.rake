desc "Draft all players that are available"
task draft: :environment do
  drafts_in_progress = Draft.in_progress

  while !drafts_in_progress.empty?
    draft = drafts_in_progress.first
    draft.player = Player.random_available_player

    if draft.save!
      puts "#{draft.team.name} acquired #{draft.player.name}"
    else
      puts "Error saving Draft #{draft.id}"
    end

    drafts_in_progress.delete(drafts_in_progress.first)
  end
end
