require 'csv'

# Generate Teams
CSV.foreach(File.expand_path('db/teams.csv')) do |row|
  if row[0] == "Team Name"
    next
  else
    Team.create(name: row[0].strip, division: row[1])
  end
end

# Generate Players
CSV.foreach(File.expand_path('db/players.csv')) do |row|
  if row[0] == "Player Name"
    next
  else
    Player.create(name: row[0].strip, position: row[1])
  end
end

# Generate Draft
CSV.foreach(File.expand_path('db/order.csv')) do |row|
  if row[0] == "Round"
    next
  else
    team = Team.where(name: Team.translate(:"#{row[2]}")).first
    Draft.create(round: row[0], pick: row[1], team_id: team.id)
  end
end
